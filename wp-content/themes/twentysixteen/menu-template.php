<?php
/**
 * Template Name: Menu
 **/
get_header();

?>
<div id="primary" class="content-area">

    <main id="main" class="site-main" role="main">
        <?php
        $currentWeekDay = date( "w" );

        global $wpdb;
        $prefix = $wpdb->prefix;
        $tableName = $prefix.'postmeta';
        $menuQuery = 'SELECT * FROM '.$tableName.' WHERE meta_key ="data_data"';

        $result = $wpdb->get_results($menuQuery);

        $currentPostId = '';
        foreach($result as $value){
            if(date("w",strtotime($value->meta_value)) == $currentWeekDay) {
                $currentPostId = $value->post_id;
            }

        }

        //Wypisanie dni tygodnia

        echo do_shortcode('[weekDays]');

        // Start the loop.
        $query = new WP_Query( array('post_type' => 'menu', 'p' => $currentPostId ) );
        while ( $query->have_posts() ) : $query->the_post();

            // Include the page content template.
            get_template_part( 'template-parts/content', 'page' );

            // If comments are open or we have at least one comment, load up the comment template.
            /*if ( comments_open() || get_comments_number() ) {
                comments_template();
            }*/

            // End of the loop.
        endwhile;
        ?>

    </main><!-- .site-main -->

    <?php get_sidebar( 'content-bottom' ); ?>

</div>



<script type="text/javascript">
    jQuery(document).ready(function($){

        jQuery( ".load_post" ).click(function() {
            var dayNumber = $(this).attr('day-number');
            var data ={
                'action': 'load_post',
                'dayNumber': dayNumber
            };
            jQuery.post('http://127.0.0.1/wordpress/wp-admin/admin-ajax.php', data, function(ajaxResponse){
                var post_data = $.parseJSON(ajaxResponse)
                $('.entry-title').html(post_data.postTitle);
                $('.entry-content > p').html(post_data.postContent);
            });

            return false;
        });

    });

</script>

<?php get_sidebar(); ?>
<?php get_footer(); ?>

