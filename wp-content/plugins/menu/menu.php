<?php
/*
Plugin Name: Menu plugin
Description: Menu plugin task
Version: 1.0
*/


//Custom Post Type
add_action( 'init', 'create_post_type' );
function create_post_type() {
    register_post_type( 'menu',
        array(
            'labels' => array(
                'name' => __( 'Menu' ),
                'singular_name' => __( 'Menu' ),
                ''
            ),
            'show_ui' => true,
            'menu_position' => 5,
            'public' => true,
            'supports' => array( 'title', 'editor', ),
            'menu_icon' => 'dashicons-welcome-widgets-menus'
        )

    );
}
//Custom field with jquery datapicker
function data_get_meta( $value ) {
    global $post;

    $field = get_post_meta( $post->ID, $value, true );
    if ( ! empty( $field ) ) {
        return is_array( $field ) ? stripslashes_deep( $field ) : stripslashes( wp_kses_decode_entities( $field ) );
    } else {
        return false;
    }
}

function data_add_meta_box() {
    add_meta_box(
        'data-data',
        __( 'Data', 'data' ),
        'data_html',
        'menu',
        'normal',
        'default'
    );
}
add_action( 'add_meta_boxes', 'data_add_meta_box' );

function data_html( $post) {
    wp_nonce_field( '_data_nonce', 'data_nonce' ); ?>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="/resources/demos/style.css">
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script>
        $( function() {
            $( "#datepicker" ).datepicker({ dateFormat: 'dd-mm-yy' }).val();
        } );
    </script>
    <p>
    <label for="data_data"><?php _e( 'Data', 'data' ); ?></label><br>
    <input type="text" name="data_data" id="datepicker" value="<?php echo data_get_meta( 'data_data' ); ?>">
    </p><?php
}

function data_save( $post_id ) {
    if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) return;
    if ( ! isset( $_POST['data_nonce'] ) || ! wp_verify_nonce( $_POST['data_nonce'], '_data_nonce' ) ) return;
    if ( ! current_user_can( 'edit_post', $post_id ) ) return;

    if ( isset( $_POST['data_data'] ) )
        update_post_meta( $post_id, 'data_data', esc_attr( $_POST['data_data'] ) );
}
add_action( 'save_post', 'data_save' );


//Shortcode
function showWeekdays(){
    $currentWeekDay = date( "w" );
    $weekdays = array( 'Niedziela', 'Poniedzialek', 'Wtorek', 'Sroda', 'Czwartek', 'Piatek', 'Sobota' );
    date_default_timezone_set('Europe/Warsaw');


    for($i = 0; $i<count($weekdays); $i++) {
        if($i == $currentWeekDay){
            echo ' <button type="button" id="load_post" day-number="'.$i.'" style="font-size: 15px;" class="currentButton load_post">' . $weekdays[$i] . '</button> ';
        }else {
            echo ' <button type="button" id="load_post'.$i.'" day-number="'.$i.'" style="font-size: 12px;" class="load_post">' . $weekdays[$i] . '</button> ';
        }
    }
}

add_shortcode('weekDays', 'showWeekdays');

//AJAX


add_action( 'wp_ajax_nopriv_load_post', 'load_post');
add_action( 'wp_ajax_load_post', 'load_post');
function load_post(){
    global $wpdb;
    $postDay = intval($_POST['dayNumber']);

    $prefix = $wpdb->prefix;
    $tableName = $prefix.'postmeta';
    $menuQuery = 'SELECT * FROM '.$tableName.' WHERE meta_key ="data_data"';

    $result = $wpdb->get_results($menuQuery);

    $currentPostId = '';
    foreach($result as $value){
        if(date("w",strtotime($value->meta_value)) == $postDay) {
            $currentPostId = $value->post_id;
        }

    }
    $tableName = $prefix.'posts';
    $newQuery = 'SELECT post_content, post_title FROM '.$tableName.' WHERE ID = "'.$currentPostId.'"';
    $result = $wpdb->get_row($newQuery);

    $newData['postTitle'] = sanitize_text_field($result->post_title);
    $newData['postContent'] = sanitize_text_field($result->post_content);

    echo json_encode($newData);



    wp_die();
}