<?php
/**
 * Podstawowa konfiguracja WordPressa.
 *
 * Skrypt wp-config.php używa tego pliku podczas instalacji.
 * Nie musisz dokonywać konfiguracji przy pomocy przeglądarki internetowej,
 * możesz też skopiować ten plik, nazwać kopię "wp-config.php"
 * i wpisać wartości ręcznie.
 *
 * Ten plik zawiera konfigurację:
 *
 * * ustawień MySQL-a,
 * * tajnych kluczy,
 * * prefiksu nazw tabel w bazie danych,
 * * ABSPATH.
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** Ustawienia MySQL-a - możesz uzyskać je od administratora Twojego serwera ** //
/** Nazwa bazy danych, której używać ma WordPress */
define('DB_NAME', 'wordpress');

/** Nazwa użytkownika bazy danych MySQL */
define('DB_USER', 'root');

/** Hasło użytkownika bazy danych MySQL */
define('DB_PASSWORD', '');

/** Nazwa hosta serwera MySQL */
define('DB_HOST', 'localhost');

/** Kodowanie bazy danych używane do stworzenia tabel w bazie danych. */
define('DB_CHARSET', 'utf8mb4');

/** Typ porównań w bazie danych. Nie zmieniaj tego ustawienia, jeśli masz jakieś wątpliwości. */
define('DB_COLLATE', '');

/**#@+
 * Unikatowe klucze uwierzytelniania i sole.
 *
 * Zmień każdy klucz tak, aby był inną, unikatową frazą!
 * Możesz wygenerować klucze przy pomocy {@link https://api.wordpress.org/secret-key/1.1/salt/ serwisu generującego tajne klucze witryny WordPress.org}
 * Klucze te mogą zostać zmienione w dowolnej chwili, aby uczynić nieważnymi wszelkie istniejące ciasteczka. Uczynienie tego zmusi wszystkich użytkowników do ponownego zalogowania się.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'N0^r#:Bf2Q/?AVjUhZH?n+Qy6LBgRPkrMAMy5c$dj:<gdV2_ccOw@h`[QcPoHZN&');
define('SECURE_AUTH_KEY',  'mlITU5KbXDLGE3_;fsmnA5aq*R&-GDoA.W>g9rBw)m]4t`{E Z3MRvc&6$EXJZ.>');
define('LOGGED_IN_KEY',    'p1 8E4V=TA}G0xe<U9b*w~iUnfI^?Ft:(o#8EdJE[JBY Lxs:aQ|dQyClaDs)M]S');
define('NONCE_KEY',        '%)]_!<8`V>_JQxM:w2 q#<YI)wIm=qEUUEr[F ;AGo<LDf?bEy@*A3 FU^n{hJ/6');
define('AUTH_SALT',        '2oMIJit(Z~A|ItfH<@MGuqn8}[Fum78+&t3Ea(z<?QxzU{LhnT4`^@aKvbvz`#a+');
define('SECURE_AUTH_SALT', 'ZV)6CGcvYZ wPUHbdiYK|qH#n836Lh%NvBKR(CX`T]irUf=|jQG;05cT<O,3ZCs0');
define('LOGGED_IN_SALT',   'M4E$aDN4ZU*HX]{$&e`-!8 y}#/}~u_/6*4F3|vM~DX|B%[6k]2FzY>QlGo&/~N#');
define('NONCE_SALT',       '-L&,nN`23`QkwOZ[IJR:]r m2Jf-;:<60Tlw((Hib&scTyn)zlPI`opI :s>IsxT');

/**#@-*/

/**
 * Prefiks tabel WordPressa w bazie danych.
 *
 * Możesz posiadać kilka instalacji WordPressa w jednej bazie danych,
 * jeżeli nadasz każdej z nich unikalny prefiks.
 * Tylko cyfry, litery i znaki podkreślenia, proszę!
 */
$table_prefix  = 'wp1_';

/**
 * Dla programistów: tryb debugowania WordPressa.
 *
 * Zmień wartość tej stałej na true, aby włączyć wyświetlanie
 * ostrzeżeń podczas modyfikowania kodu WordPressa.
 * Wielce zalecane jest, aby twórcy wtyczek oraz motywów używali
 * WP_DEBUG podczas pracy nad nimi.
 *
 * Aby uzyskać informacje o innych stałych, które mogą zostać użyte
 * do debugowania, przejdź na stronę Kodeksu WordPressa.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* To wszystko, zakończ edycję w tym miejscu! Miłego blogowania! */

/** Absolutna ścieżka do katalogu WordPressa. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Ustawia zmienne WordPressa i dołączane pliki. */
require_once(ABSPATH . 'wp-settings.php');
